package ru.tsc.goloshchapov.tm.util;

import ru.tsc.goloshchapov.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (RuntimeException e) {
            throw new IndexIncorrectException(value);
        }
    }

}
