package ru.tsc.goloshchapov.tm;

import ru.tsc.goloshchapov.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
