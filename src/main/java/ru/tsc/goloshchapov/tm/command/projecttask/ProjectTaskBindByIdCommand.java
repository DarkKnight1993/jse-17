package ru.tsc.goloshchapov.tm.command.projecttask;

import ru.tsc.goloshchapov.tm.command.AbstractProjectTaskCommand;
import ru.tsc.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public class ProjectTaskBindByIdCommand extends AbstractProjectTaskCommand {
    @Override
    public String name() {
        return "project-task-bind-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Bind task to project by id";
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT BY ID]");
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        final Task taskBinded = serviceLocator.getProjectTaskService().bindTaskById(projectId, taskId);
        if (taskBinded == null) throw new TaskNotFoundException();
    }

}
