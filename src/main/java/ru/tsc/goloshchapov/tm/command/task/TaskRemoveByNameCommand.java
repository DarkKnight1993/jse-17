package ru.tsc.goloshchapov.tm.command.task;

import ru.tsc.goloshchapov.tm.command.AbstractTaskCommand;
import ru.tsc.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {
    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by name";
    }

    @Override
    public void execute() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        Task task = serviceLocator.getTaskService().findByName(name);
        if (task == null) throw new TaskNotFoundException();
        final Task taskRemoved = serviceLocator.getTaskService().removeByName(name);
        if (taskRemoved == null) throw new TaskNotFoundException();
    }

}
