package ru.tsc.goloshchapov.tm.command.project;

import ru.tsc.goloshchapov.tm.command.AbstractProjectCommand;
import ru.tsc.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public class ProjectStartByIndexCommand extends AbstractProjectCommand {
    @Override
    public String name() {
        return "project-start-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start project by index";
    }

    @Override
    public void execute() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().startByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

}
