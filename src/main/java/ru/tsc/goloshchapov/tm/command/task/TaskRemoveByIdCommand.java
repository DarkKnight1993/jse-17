package ru.tsc.goloshchapov.tm.command.task;

import ru.tsc.goloshchapov.tm.command.AbstractTaskCommand;
import ru.tsc.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends AbstractTaskCommand {
    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by id";
    }

    @Override
    public void execute() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        if (!serviceLocator.getTaskService().existsById(id)) throw new TaskNotFoundException();
        final Task taskRemoved = serviceLocator.getTaskService().removeById(id);
        if (taskRemoved == null) throw new TaskNotFoundException();
    }

}
